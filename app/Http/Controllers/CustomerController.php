<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\CustomerModel;
use Exception;

class CustomerController extends Controller {

    private $customerModel;

    public function __construct() {
        //$this->middleware('cors');
        $this->customerModel = new CustomerModel();
    }

    public function loadCustomers() {
        try {
            $users = $this->customerModel->getCustomerList();
            return response()->json(['data' => $users], 200)
                            ->header('Content-Type', 'text/json');
        } catch (Exception $e) {
            return response()->json(['data' => 'no ddata found'], 403)
                            ->header('Content-Type', 'text/json');
        }
    }

}
