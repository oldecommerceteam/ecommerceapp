<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Exception;

class CustomerModel extends Model {

    protected $table = 'customer';

    public function getCustomerList() {
        $users = DB::table($this->table)->get()->toArray();
        if (empty($users)) {
            throw new Exception("No data found");
        }
        return $users;
    }

}
